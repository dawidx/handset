<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="xc9500xl" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="PISO_CLK" />
        <signal name="XLXN_16" />
        <signal name="XLXN_20" />
        <signal name="Encoder_Z" />
        <signal name="Vcc" />
        <signal name="XLXN_31" />
        <signal name="XLXN_36" />
        <signal name="XLXN_38" />
        <signal name="XLXN_41" />
        <signal name="XLXN_42" />
        <signal name="XLXN_44" />
        <signal name="XLXN_45" />
        <signal name="PISO_BUTTON_CHANGE_PULSE" />
        <signal name="WTK0" />
        <signal name="PISO_CLK_ENABLE" />
        <signal name="XLXN_90" />
        <signal name="XLXN_91" />
        <signal name="XLXN_93" />
        <signal name="XLXN_94" />
        <signal name="XLXN_95" />
        <signal name="Encoder_Y" />
        <signal name="Encoder_X" />
        <port polarity="Input" name="PISO_CLK" />
        <port polarity="Input" name="Encoder_Z" />
        <port polarity="Input" name="PISO_BUTTON_CHANGE_PULSE" />
        <port polarity="Output" name="WTK0" />
        <port polarity="Input" name="PISO_CLK_ENABLE" />
        <port polarity="Input" name="Encoder_Y" />
        <port polarity="Input" name="Encoder_X" />
        <blockdef name="m2_1">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="96" y1="-64" y2="-192" x1="96" />
            <line x2="96" y1="-96" y2="-64" x1="256" />
            <line x2="256" y1="-160" y2="-96" x1="256" />
            <line x2="256" y1="-192" y2="-160" x1="96" />
            <line x2="96" y1="-32" y2="-32" x1="176" />
            <line x2="176" y1="-80" y2="-32" x1="176" />
            <line x2="96" y1="-32" y2="-32" x1="0" />
            <line x2="256" y1="-128" y2="-128" x1="320" />
            <line x2="96" y1="-96" y2="-96" x1="0" />
            <line x2="96" y1="-160" y2="-160" x1="0" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="32" y1="-64" y2="-64" x1="96" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="64" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-96" x1="64" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="64" y1="-64" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="fdce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <block symbolname="m2_1" name="XLXI_13">
            <blockpin signalname="Encoder_Z" name="D0" />
            <blockpin signalname="XLXN_16" name="D1" />
            <blockpin signalname="PISO_BUTTON_CHANGE_PULSE" name="S0" />
            <blockpin signalname="XLXN_20" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_14">
            <blockpin signalname="Encoder_Y" name="D0" />
            <blockpin signalname="XLXN_36" name="D1" />
            <blockpin signalname="PISO_BUTTON_CHANGE_PULSE" name="S0" />
            <blockpin signalname="XLXN_31" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_15">
            <blockpin signalname="Encoder_X" name="D0" />
            <blockpin signalname="XLXN_38" name="D1" />
            <blockpin signalname="PISO_BUTTON_CHANGE_PULSE" name="S0" />
            <blockpin signalname="XLXN_41" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_16">
            <blockpin signalname="XLXN_45" name="D0" />
            <blockpin signalname="XLXN_42" name="D1" />
            <blockpin signalname="PISO_BUTTON_CHANGE_PULSE" name="S0" />
            <blockpin signalname="XLXN_44" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_26">
            <blockpin signalname="Vcc" name="P" />
        </block>
        <block symbolname="gnd" name="XLXI_27">
            <blockpin signalname="XLXN_45" name="G" />
        </block>
        <block symbolname="fdce" name="XLXI_30">
            <blockpin signalname="PISO_CLK" name="C" />
            <blockpin signalname="PISO_CLK_ENABLE" name="CE" />
            <blockpin signalname="XLXN_90" name="CLR" />
            <blockpin signalname="XLXN_20" name="D" />
            <blockpin signalname="XLXN_36" name="Q" />
        </block>
        <block symbolname="gnd" name="XLXI_31">
            <blockpin signalname="XLXN_90" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_32">
            <blockpin signalname="XLXN_91" name="G" />
        </block>
        <block symbolname="fdce" name="XLXI_33">
            <blockpin signalname="PISO_CLK" name="C" />
            <blockpin signalname="PISO_CLK_ENABLE" name="CE" />
            <blockpin signalname="XLXN_91" name="CLR" />
            <blockpin signalname="Vcc" name="D" />
            <blockpin signalname="XLXN_16" name="Q" />
        </block>
        <block symbolname="fdce" name="XLXI_34">
            <blockpin signalname="PISO_CLK" name="C" />
            <blockpin signalname="PISO_CLK_ENABLE" name="CE" />
            <blockpin signalname="XLXN_93" name="CLR" />
            <blockpin signalname="XLXN_31" name="D" />
            <blockpin signalname="XLXN_38" name="Q" />
        </block>
        <block symbolname="fdce" name="XLXI_35">
            <blockpin signalname="PISO_CLK" name="C" />
            <blockpin signalname="PISO_CLK_ENABLE" name="CE" />
            <blockpin signalname="XLXN_94" name="CLR" />
            <blockpin signalname="XLXN_41" name="D" />
            <blockpin signalname="XLXN_42" name="Q" />
        </block>
        <block symbolname="fdce" name="XLXI_36">
            <blockpin signalname="PISO_CLK" name="C" />
            <blockpin signalname="PISO_CLK_ENABLE" name="CE" />
            <blockpin signalname="XLXN_95" name="CLR" />
            <blockpin signalname="XLXN_44" name="D" />
            <blockpin signalname="WTK0" name="Q" />
        </block>
        <block symbolname="gnd" name="XLXI_37">
            <blockpin signalname="XLXN_93" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_38">
            <blockpin signalname="XLXN_94" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_39">
            <blockpin signalname="XLXN_95" name="G" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <iomarker fontsize="28" x="256" y="1376" name="PISO_CLK" orien="R180" />
        <branch name="PISO_CLK">
            <wire x2="368" y1="1376" y2="1376" x1="256" />
            <wire x2="1136" y1="1376" y2="1376" x1="368" />
            <wire x2="1680" y1="1376" y2="1376" x1="1136" />
            <wire x2="2336" y1="1376" y2="1376" x1="1680" />
            <wire x2="2944" y1="1376" y2="1376" x1="2336" />
            <wire x2="368" y1="1184" y2="1376" x1="368" />
            <wire x2="512" y1="1184" y2="1184" x1="368" />
            <wire x2="1184" y1="1184" y2="1184" x1="1136" />
            <wire x2="1136" y1="1184" y2="1376" x1="1136" />
            <wire x2="1776" y1="1168" y2="1168" x1="1680" />
            <wire x2="1680" y1="1168" y2="1376" x1="1680" />
            <wire x2="2448" y1="1168" y2="1168" x1="2336" />
            <wire x2="2336" y1="1168" y2="1376" x1="2336" />
            <wire x2="2944" y1="1168" y2="1376" x1="2944" />
            <wire x2="3088" y1="1168" y2="1168" x1="2944" />
        </branch>
        <branch name="Encoder_Z">
            <wire x2="848" y1="416" y2="416" x1="656" />
        </branch>
        <branch name="Vcc">
            <wire x2="272" y1="1024" y2="1056" x1="272" />
            <wire x2="512" y1="1056" y2="1056" x1="272" />
        </branch>
        <branch name="XLXN_31">
            <wire x2="1760" y1="880" y2="1040" x1="1760" />
            <wire x2="1776" y1="1040" y2="1040" x1="1760" />
            <wire x2="1808" y1="880" y2="880" x1="1760" />
            <wire x2="1808" y1="448" y2="448" x1="1792" />
            <wire x2="1808" y1="448" y2="880" x1="1808" />
        </branch>
        <iomarker fontsize="28" x="656" y="416" name="Encoder_Z" orien="R180" />
        <instance x="208" y="1024" name="XLXI_26" orien="R0" />
        <iomarker fontsize="28" x="336" y="544" name="PISO_BUTTON_CHANGE_PULSE" orien="R180" />
        <instance x="2528" y="752" name="XLXI_27" orien="R0" />
        <branch name="XLXN_44">
            <wire x2="3072" y1="912" y2="1040" x1="3072" />
            <wire x2="3088" y1="1040" y2="1040" x1="3072" />
            <wire x2="3200" y1="912" y2="912" x1="3072" />
            <wire x2="3200" y1="448" y2="448" x1="3120" />
            <wire x2="3200" y1="448" y2="912" x1="3200" />
        </branch>
        <branch name="WTK0">
            <wire x2="3504" y1="1040" y2="1040" x1="3472" />
        </branch>
        <iomarker fontsize="28" x="3504" y="1040" name="WTK0" orien="R180" />
        <branch name="PISO_CLK_ENABLE">
            <wire x2="400" y1="1472" y2="1472" x1="256" />
            <wire x2="1056" y1="1472" y2="1472" x1="400" />
            <wire x2="1600" y1="1472" y2="1472" x1="1056" />
            <wire x2="2304" y1="1472" y2="1472" x1="1600" />
            <wire x2="2928" y1="1472" y2="1472" x1="2304" />
            <wire x2="512" y1="1120" y2="1120" x1="400" />
            <wire x2="400" y1="1120" y2="1472" x1="400" />
            <wire x2="1056" y1="1120" y2="1472" x1="1056" />
            <wire x2="1184" y1="1120" y2="1120" x1="1056" />
            <wire x2="1776" y1="1104" y2="1104" x1="1600" />
            <wire x2="1600" y1="1104" y2="1472" x1="1600" />
            <wire x2="2448" y1="1104" y2="1104" x1="2304" />
            <wire x2="2304" y1="1104" y2="1472" x1="2304" />
            <wire x2="2928" y1="1104" y2="1472" x1="2928" />
            <wire x2="3088" y1="1104" y2="1104" x1="2928" />
        </branch>
        <iomarker fontsize="28" x="256" y="1472" name="PISO_CLK_ENABLE" orien="R180" />
        <instance x="1184" y="1312" name="XLXI_30" orien="R0" />
        <instance x="1120" y="1680" name="XLXI_31" orien="R0" />
        <branch name="XLXN_90">
            <wire x2="1184" y1="1280" y2="1552" x1="1184" />
        </branch>
        <instance x="432" y="1696" name="XLXI_32" orien="R0" />
        <branch name="XLXN_91">
            <wire x2="512" y1="1280" y2="1280" x1="496" />
            <wire x2="496" y1="1280" y2="1568" x1="496" />
        </branch>
        <instance x="512" y="1312" name="XLXI_33" orien="R0" />
        <instance x="1776" y="1296" name="XLXI_34" orien="R0" />
        <instance x="2448" y="1296" name="XLXI_35" orien="R0" />
        <instance x="3088" y="1296" name="XLXI_36" orien="R0" />
        <branch name="XLXN_93">
            <wire x2="1776" y1="1264" y2="1568" x1="1776" />
        </branch>
        <branch name="XLXN_95">
            <wire x2="3088" y1="1264" y2="1552" x1="3088" />
        </branch>
        <instance x="2384" y="1680" name="XLXI_38" orien="R0" />
        <branch name="XLXN_94">
            <wire x2="2448" y1="1264" y2="1552" x1="2448" />
        </branch>
        <instance x="1712" y="1696" name="XLXI_37" orien="R0" />
        <instance x="3024" y="1680" name="XLXI_39" orien="R0" />
        <iomarker fontsize="28" x="1440" y="416" name="Encoder_Y" orien="R180" />
        <iomarker fontsize="28" x="2080" y="416" name="Encoder_X" orien="R180" />
        <instance x="848" y="576" name="XLXI_13" orien="R0" />
        <branch name="XLXN_16">
            <wire x2="848" y1="480" y2="480" x1="784" />
            <wire x2="784" y1="480" y2="624" x1="784" />
            <wire x2="912" y1="624" y2="624" x1="784" />
            <wire x2="912" y1="624" y2="1056" x1="912" />
            <wire x2="912" y1="1056" y2="1056" x1="896" />
        </branch>
        <branch name="XLXN_20">
            <wire x2="1232" y1="448" y2="448" x1="1168" />
            <wire x2="1232" y1="448" y2="608" x1="1232" />
            <wire x2="1232" y1="608" y2="608" x1="1168" />
            <wire x2="1168" y1="608" y2="1056" x1="1168" />
            <wire x2="1184" y1="1056" y2="1056" x1="1168" />
        </branch>
        <branch name="XLXN_36">
            <wire x2="1392" y1="480" y2="608" x1="1392" />
            <wire x2="1584" y1="608" y2="608" x1="1392" />
            <wire x2="1584" y1="608" y2="1056" x1="1584" />
            <wire x2="1472" y1="480" y2="480" x1="1392" />
            <wire x2="1584" y1="1056" y2="1056" x1="1568" />
        </branch>
        <branch name="Encoder_Y">
            <wire x2="1472" y1="416" y2="416" x1="1440" />
        </branch>
        <instance x="1472" y="576" name="XLXI_14" orien="R0" />
        <branch name="XLXN_41">
            <wire x2="2496" y1="448" y2="448" x1="2432" />
            <wire x2="2496" y1="448" y2="608" x1="2496" />
            <wire x2="2432" y1="608" y2="1040" x1="2432" />
            <wire x2="2448" y1="1040" y2="1040" x1="2432" />
            <wire x2="2496" y1="608" y2="608" x1="2432" />
        </branch>
        <branch name="PISO_BUTTON_CHANGE_PULSE">
            <wire x2="832" y1="544" y2="544" x1="336" />
            <wire x2="848" y1="544" y2="544" x1="832" />
            <wire x2="1440" y1="544" y2="544" x1="832" />
            <wire x2="1472" y1="544" y2="544" x1="1440" />
            <wire x2="2096" y1="544" y2="544" x1="1440" />
            <wire x2="2112" y1="544" y2="544" x1="2096" />
            <wire x2="2800" y1="544" y2="544" x1="2096" />
        </branch>
        <branch name="XLXN_38">
            <wire x2="2032" y1="480" y2="624" x1="2032" />
            <wire x2="2176" y1="624" y2="624" x1="2032" />
            <wire x2="2176" y1="624" y2="1040" x1="2176" />
            <wire x2="2112" y1="480" y2="480" x1="2032" />
            <wire x2="2176" y1="1040" y2="1040" x1="2160" />
        </branch>
        <branch name="Encoder_X">
            <wire x2="2096" y1="416" y2="416" x1="2080" />
            <wire x2="2112" y1="416" y2="416" x1="2096" />
        </branch>
        <instance x="2112" y="576" name="XLXI_15" orien="R0" />
        <instance x="2800" y="576" name="XLXI_16" orien="R0" />
        <branch name="XLXN_45">
            <wire x2="2800" y1="416" y2="416" x1="2592" />
            <wire x2="2592" y1="416" y2="624" x1="2592" />
        </branch>
        <branch name="XLXN_42">
            <wire x2="2800" y1="480" y2="480" x1="2720" />
            <wire x2="2720" y1="480" y2="608" x1="2720" />
            <wire x2="2848" y1="608" y2="608" x1="2720" />
            <wire x2="2848" y1="608" y2="1040" x1="2848" />
            <wire x2="2848" y1="1040" y2="1040" x1="2832" />
        </branch>
    </sheet>
</drawing>