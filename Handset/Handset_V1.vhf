--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.2
--  \   \         Application : sch2hdl
--  /   /         Filename : Handset_V1.vhf
-- /___/   /\     Timestamp : 04/26/2013 15:07:46
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family xc9500xl -flat -suppress -vhdl H:/Robot/Handset/Handset/Handset_V1.vhf -w H:/Robot/Handset/Handset/Handset_V1.sch
--Design Name: Handset_V1
--Device: xc9500xl
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity FDC_MXILINX_Handset_V1 is
   generic( INIT : bit :=  '0');
   port ( C   : in    std_logic; 
          CLR : in    std_logic; 
          D   : in    std_logic; 
          Q   : out   std_logic);
end FDC_MXILINX_Handset_V1;

architecture BEHAVIORAL of FDC_MXILINX_Handset_V1 is
   attribute BOX_TYPE   : string ;
   signal XLXN_5 : std_logic;
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component FDCP
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             PRE : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDCP : component is "BLACK_BOX";
   
begin
   I_36_55 : GND
      port map (G=>XLXN_5);
   
   U0 : FDCP
   generic map( INIT => INIT)
      port map (C=>C,
                CLR=>CLR,
                D=>D,
                PRE=>XLXN_5,
                Q=>Q);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Handset_V1 is
   port ( CLK : in    std_logic; 
          CLR : in    std_logic; 
          D0  : in    std_logic; 
          D1  : in    std_logic; 
          D2  : in    std_logic; 
          D3  : in    std_logic; 
          D4  : in    std_logic; 
          D5  : in    std_logic; 
          D6  : in    std_logic; 
          D7  : in    std_logic; 
          D8  : in    std_logic; 
          D9  : in    std_logic; 
          D10 : in    std_logic; 
          D11 : in    std_logic; 
          Q0  : out   std_logic; 
          Q1  : out   std_logic; 
          Q2  : out   std_logic; 
          Q3  : out   std_logic; 
          Q4  : out   std_logic; 
          Q5  : out   std_logic; 
          Q6  : out   std_logic; 
          Q7  : out   std_logic; 
          Q8  : out   std_logic; 
          Q9  : out   std_logic; 
          Q10 : out   std_logic; 
          Q11 : out   std_logic);
end Handset_V1;

architecture BEHAVIORAL of Handset_V1 is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   component FDC_MXILINX_Handset_V1
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component NAND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NAND2 : component is "BLACK_BOX";
   
   component NOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NOR2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_1 : label is "XLXI_1_3";
   attribute HU_SET of XLXI_2 : label is "XLXI_2_0";
   attribute HU_SET of XLXI_3 : label is "XLXI_3_1";
   attribute HU_SET of XLXI_4 : label is "XLXI_4_2";
   attribute HU_SET of XLXI_18 : label is "XLXI_18_4";
   attribute HU_SET of XLXI_19 : label is "XLXI_19_5";
   attribute HU_SET of XLXI_20 : label is "XLXI_20_6";
   attribute HU_SET of XLXI_21 : label is "XLXI_21_7";
begin
   XLXI_1 : FDC_MXILINX_Handset_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D0,
                Q=>Q0);
   
   XLXI_2 : FDC_MXILINX_Handset_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D1,
                Q=>Q1);
   
   XLXI_3 : FDC_MXILINX_Handset_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D2,
                Q=>Q2);
   
   XLXI_4 : FDC_MXILINX_Handset_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D3,
                Q=>Q3);
   
   XLXI_18 : FDC_MXILINX_Handset_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D5,
                Q=>Q5);
   
   XLXI_19 : FDC_MXILINX_Handset_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D6,
                Q=>Q6);
   
   XLXI_20 : FDC_MXILINX_Handset_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D7,
                Q=>Q7);
   
   XLXI_21 : FDC_MXILINX_Handset_V1
      port map (C=>CLK,
                CLR=>CLR,
                D=>D4,
                Q=>Q4);
   
   XLXI_33 : AND2
      port map (I0=>D9,
                I1=>D8,
                O=>Q8);
   
   XLXI_34 : NAND2
      port map (I0=>D11,
                I1=>D10,
                O=>Q9);
   
   XLXI_35 : NOR2
      port map (I0=>D10,
                I1=>D8,
                O=>Q10);
   
   XLXI_36 : OR2
      port map (I0=>D8,
                I1=>D11,
                O=>Q11);
   
end BEHAVIORAL;


