<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="xc9500xl" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_15" />
        <signal name="XLXN_19" />
        <signal name="XLXN_24" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="PISO_BUTTON_CHANGE_PULSE" />
        <signal name="XLXN_31" />
        <port polarity="Output" name="PISO_BUTTON_CHANGE_PULSE" />
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <block symbolname="fd" name="XLXI_11">
            <blockpin name="C" />
            <blockpin signalname="XLXN_31" name="D" />
            <blockpin signalname="PISO_BUTTON_CHANGE_PULSE" name="Q" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <iomarker fontsize="28" x="2064" y="400" name="PISO_BUTTON_CHANGE_PULSE" orien="R0" />
        <branch name="PISO_BUTTON_CHANGE_PULSE">
            <wire x2="1744" y1="672" y2="672" x1="1472" />
            <wire x2="2064" y1="400" y2="400" x1="1744" />
            <wire x2="1744" y1="400" y2="672" x1="1744" />
        </branch>
        <instance x="1088" y="928" name="XLXI_11" orien="R0" />
        <branch name="XLXN_31">
            <wire x2="1088" y1="672" y2="672" x1="816" />
        </branch>
    </sheet>
</drawing>