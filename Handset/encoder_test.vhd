-- Vhdl test bench created from schematic H:\Robot\Handset\Handset\encoder.sch - Fri Apr 26 11:46:14 2013
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY encoder_encoder_sch_tb IS
END encoder_encoder_sch_tb;
ARCHITECTURE behavioral OF encoder_encoder_sch_tb IS 

   COMPONENT encoder
   PORT( D7	:	IN	STD_LOGIC; 
          D6	:	IN	STD_LOGIC; 
          D5	:	IN	STD_LOGIC; 
          D4	:	IN	STD_LOGIC; 
          D3	:	IN	STD_LOGIC; 
          D2	:	IN	STD_LOGIC; 
          D1	:	IN	STD_LOGIC; 
          D0	:	IN	STD_LOGIC);
   END COMPONENT;

   SIGNAL D7	:	STD_LOGIC;
   SIGNAL D6	:	STD_LOGIC;
   SIGNAL D5	:	STD_LOGIC;
   SIGNAL D4	:	STD_LOGIC;
   SIGNAL D3	:	STD_LOGIC;
   SIGNAL D2	:	STD_LOGIC;
   SIGNAL D1	:	STD_LOGIC;
   SIGNAL D0	:	STD_LOGIC;

BEGIN

   UUT: encoder PORT MAP(
		D7 => D7, 
		D6 => D6, 
		D5 => D5, 
		D4 => D4, 
		D3 => D3, 
		D2 => D2, 
		D1 => D1, 
		D0 => D0
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
		D0 <= '0'; wait for 100 ns;
		D1 <= '0'; wait for 100 ns;
		D2 <= '0'; wait for 100 ns;
		D3 <= '0'; wait for 100 ns;
		D4 <= '0'; wait for 100 ns;
		D5 <= '0'; wait for 100 ns;
		D6 <= '0'; wait for 100 ns;
		D7 <= '0'; wait for 100 ns;
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
