--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.2
--  \   \         Application : sch2hdl
--  /   /         Filename : Top_Level.vhf
-- /___/   /\     Timestamp : 04/26/2013 15:07:46
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family xc9500xl -flat -suppress -vhdl H:/Robot/Handset/Handset/Top_Level.vhf -w H:/Robot/Handset/Handset/Top_Level.sch
--Design Name: Top_Level
--Device: xc9500xl
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity FDC_MXILINX_Top_Level is
   generic( INIT : bit :=  '0');
   port ( C   : in    std_logic; 
          CLR : in    std_logic; 
          D   : in    std_logic; 
          Q   : out   std_logic);
end FDC_MXILINX_Top_Level;

architecture BEHAVIORAL of FDC_MXILINX_Top_Level is
   attribute BOX_TYPE   : string ;
   signal XLXN_5 : std_logic;
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component FDCP
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             PRE : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDCP : component is "BLACK_BOX";
   
begin
   I_36_55 : GND
      port map (G=>XLXN_5);
   
   U0 : FDCP
   generic map( INIT => INIT)
      port map (C=>C,
                CLR=>CLR,
                D=>D,
                PRE=>XLXN_5,
                Q=>Q);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Handset_V1_MUSER_Top_Level is
   port ( CLK : in    std_logic; 
          CLR : in    std_logic; 
          D0  : in    std_logic; 
          D1  : in    std_logic; 
          D2  : in    std_logic; 
          D3  : in    std_logic; 
          D4  : in    std_logic; 
          D5  : in    std_logic; 
          D6  : in    std_logic; 
          D7  : in    std_logic; 
          D8  : in    std_logic; 
          D9  : in    std_logic; 
          D10 : in    std_logic; 
          D11 : in    std_logic; 
          Q0  : out   std_logic; 
          Q1  : out   std_logic; 
          Q2  : out   std_logic; 
          Q3  : out   std_logic; 
          Q4  : out   std_logic; 
          Q5  : out   std_logic; 
          Q6  : out   std_logic; 
          Q7  : out   std_logic; 
          Q8  : out   std_logic; 
          Q9  : out   std_logic; 
          Q10 : out   std_logic; 
          Q11 : out   std_logic);
end Handset_V1_MUSER_Top_Level;

architecture BEHAVIORAL of Handset_V1_MUSER_Top_Level is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   component FDC_MXILINX_Top_Level
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component NAND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NAND2 : component is "BLACK_BOX";
   
   component NOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NOR2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_1 : label is "XLXI_1_11";
   attribute HU_SET of XLXI_2 : label is "XLXI_2_8";
   attribute HU_SET of XLXI_3 : label is "XLXI_3_9";
   attribute HU_SET of XLXI_4 : label is "XLXI_4_10";
   attribute HU_SET of XLXI_18 : label is "XLXI_18_12";
   attribute HU_SET of XLXI_19 : label is "XLXI_19_13";
   attribute HU_SET of XLXI_20 : label is "XLXI_20_14";
   attribute HU_SET of XLXI_21 : label is "XLXI_21_15";
begin
   XLXI_1 : FDC_MXILINX_Top_Level
      port map (C=>CLK,
                CLR=>CLR,
                D=>D0,
                Q=>Q0);
   
   XLXI_2 : FDC_MXILINX_Top_Level
      port map (C=>CLK,
                CLR=>CLR,
                D=>D1,
                Q=>Q1);
   
   XLXI_3 : FDC_MXILINX_Top_Level
      port map (C=>CLK,
                CLR=>CLR,
                D=>D2,
                Q=>Q2);
   
   XLXI_4 : FDC_MXILINX_Top_Level
      port map (C=>CLK,
                CLR=>CLR,
                D=>D3,
                Q=>Q3);
   
   XLXI_18 : FDC_MXILINX_Top_Level
      port map (C=>CLK,
                CLR=>CLR,
                D=>D5,
                Q=>Q5);
   
   XLXI_19 : FDC_MXILINX_Top_Level
      port map (C=>CLK,
                CLR=>CLR,
                D=>D6,
                Q=>Q6);
   
   XLXI_20 : FDC_MXILINX_Top_Level
      port map (C=>CLK,
                CLR=>CLR,
                D=>D7,
                Q=>Q7);
   
   XLXI_21 : FDC_MXILINX_Top_Level
      port map (C=>CLK,
                CLR=>CLR,
                D=>D4,
                Q=>Q4);
   
   XLXI_33 : AND2
      port map (I0=>D9,
                I1=>D8,
                O=>Q8);
   
   XLXI_34 : NAND2
      port map (I0=>D11,
                I1=>D10,
                O=>Q9);
   
   XLXI_35 : NOR2
      port map (I0=>D10,
                I1=>D8,
                O=>Q10);
   
   XLXI_36 : OR2
      port map (I0=>D8,
                I1=>D11,
                O=>Q11);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Top_Level is
   port ( B1   : in    std_logic; 
          B2   : in    std_logic; 
          B3   : in    std_logic; 
          B4   : in    std_logic; 
          B5   : in    std_logic; 
          B6   : in    std_logic; 
          B7   : in    std_logic; 
          B8   : in    std_logic; 
          CLK  : in    std_logic; 
          CLR  : in    std_logic; 
          WRX0 : in    std_logic; 
          WRX1 : in    std_logic; 
          WRX2 : in    std_logic; 
          WRX3 : in    std_logic; 
          L1   : out   std_logic; 
          L2   : out   std_logic; 
          L3   : out   std_logic; 
          L4   : out   std_logic; 
          L5   : out   std_logic; 
          L6   : out   std_logic; 
          L7   : out   std_logic; 
          L8   : out   std_logic; 
          WTX0 : out   std_logic; 
          WTX1 : out   std_logic; 
          WTX2 : out   std_logic; 
          WTX3 : out   std_logic);
end Top_Level;

architecture BEHAVIORAL of Top_Level is
   attribute BOX_TYPE   : string ;
   attribute SLEW       : string ;
   signal XLXN_128 : std_logic;
   signal XLXN_129 : std_logic;
   signal XLXN_130 : std_logic;
   signal XLXN_131 : std_logic;
   signal XLXN_132 : std_logic;
   signal XLXN_133 : std_logic;
   signal XLXN_134 : std_logic;
   signal XLXN_135 : std_logic;
   signal XLXN_136 : std_logic;
   signal XLXN_137 : std_logic;
   signal XLXN_158 : std_logic;
   signal XLXN_159 : std_logic;
   signal XLXN_160 : std_logic;
   signal XLXN_161 : std_logic;
   signal XLXN_239 : std_logic;
   signal XLXN_240 : std_logic;
   signal XLXN_241 : std_logic;
   signal XLXN_242 : std_logic;
   signal XLXN_243 : std_logic;
   signal XLXN_244 : std_logic;
   signal XLXN_245 : std_logic;
   signal XLXN_246 : std_logic;
   signal XLXN_253 : std_logic;
   signal XLXN_254 : std_logic;
   signal XLXN_255 : std_logic;
   signal XLXN_256 : std_logic;
   component IBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of IBUF : component is "BLACK_BOX";
   
   component OBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute SLEW of OBUF : component is "SLOW";
   attribute BOX_TYPE of OBUF : component is "BLACK_BOX";
   
   component BUFGSR
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUFGSR : component is "BLACK_BOX";
   
   component BUFG
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUFG : component is "BLACK_BOX";
   
   component Handset_V1_MUSER_Top_Level
      port ( CLK : in    std_logic; 
             CLR : in    std_logic; 
             D0  : in    std_logic; 
             D1  : in    std_logic; 
             D10 : in    std_logic; 
             D11 : in    std_logic; 
             D2  : in    std_logic; 
             D3  : in    std_logic; 
             D4  : in    std_logic; 
             D5  : in    std_logic; 
             D6  : in    std_logic; 
             D7  : in    std_logic; 
             D8  : in    std_logic; 
             D9  : in    std_logic; 
             Q0  : out   std_logic; 
             Q1  : out   std_logic; 
             Q10 : out   std_logic; 
             Q11 : out   std_logic; 
             Q2  : out   std_logic; 
             Q3  : out   std_logic; 
             Q4  : out   std_logic; 
             Q5  : out   std_logic; 
             Q6  : out   std_logic; 
             Q7  : out   std_logic; 
             Q8  : out   std_logic; 
             Q9  : out   std_logic);
   end component;
   
begin
   XLXI_1 : IBUF
      port map (I=>WRX0,
                O=>XLXN_158);
   
   XLXI_2 : IBUF
      port map (I=>WRX1,
                O=>XLXN_159);
   
   XLXI_3 : IBUF
      port map (I=>WRX2,
                O=>XLXN_160);
   
   XLXI_4 : IBUF
      port map (I=>WRX3,
                O=>XLXN_161);
   
   XLXI_5 : IBUF
      port map (I=>B8,
                O=>XLXN_137);
   
   XLXI_6 : IBUF
      port map (I=>B1,
                O=>XLXN_130);
   
   XLXI_7 : IBUF
      port map (I=>B2,
                O=>XLXN_131);
   
   XLXI_8 : IBUF
      port map (I=>B3,
                O=>XLXN_132);
   
   XLXI_9 : IBUF
      port map (I=>B4,
                O=>XLXN_133);
   
   XLXI_10 : IBUF
      port map (I=>B5,
                O=>XLXN_134);
   
   XLXI_11 : IBUF
      port map (I=>B6,
                O=>XLXN_135);
   
   XLXI_12 : IBUF
      port map (I=>B7,
                O=>XLXN_136);
   
   XLXI_19 : OBUF
      port map (I=>XLXN_239,
                O=>L1);
   
   XLXI_20 : OBUF
      port map (I=>XLXN_240,
                O=>L2);
   
   XLXI_21 : OBUF
      port map (I=>XLXN_241,
                O=>L3);
   
   XLXI_22 : OBUF
      port map (I=>XLXN_242,
                O=>L4);
   
   XLXI_23 : OBUF
      port map (I=>XLXN_243,
                O=>L5);
   
   XLXI_24 : OBUF
      port map (I=>XLXN_244,
                O=>L6);
   
   XLXI_25 : OBUF
      port map (I=>XLXN_245,
                O=>L7);
   
   XLXI_26 : OBUF
      port map (I=>XLXN_246,
                O=>L8);
   
   XLXI_27 : OBUF
      port map (I=>XLXN_253,
                O=>WTX0);
   
   XLXI_28 : OBUF
      port map (I=>XLXN_254,
                O=>WTX1);
   
   XLXI_29 : OBUF
      port map (I=>XLXN_255,
                O=>WTX2);
   
   XLXI_30 : OBUF
      port map (I=>XLXN_256,
                O=>WTX3);
   
   XLXI_33 : BUFGSR
      port map (I=>CLR,
                O=>XLXN_129);
   
   XLXI_34 : BUFG
      port map (I=>CLK,
                O=>XLXN_128);
   
   XLXI_35 : Handset_V1_MUSER_Top_Level
      port map (CLK=>XLXN_128,
                CLR=>XLXN_129,
                D0=>XLXN_130,
                D1=>XLXN_131,
                D2=>XLXN_132,
                D3=>XLXN_133,
                D4=>XLXN_134,
                D5=>XLXN_135,
                D6=>XLXN_136,
                D7=>XLXN_137,
                D8=>XLXN_158,
                D9=>XLXN_159,
                D10=>XLXN_160,
                D11=>XLXN_161,
                Q0=>XLXN_239,
                Q1=>XLXN_240,
                Q2=>XLXN_241,
                Q3=>XLXN_242,
                Q4=>XLXN_243,
                Q5=>XLXN_244,
                Q6=>XLXN_245,
                Q7=>XLXN_246,
                Q8=>XLXN_253,
                Q9=>XLXN_254,
                Q10=>XLXN_255,
                Q11=>XLXN_256);
   
end BEHAVIORAL;


