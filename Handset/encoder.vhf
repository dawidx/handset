--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.2
--  \   \         Application : sch2hdl
--  /   /         Filename : encoder.vhf
-- /___/   /\     Timestamp : 04/26/2013 12:00:00
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family xc9500xl -flat -suppress -vhdl H:/Robot/Handset/Handset/encoder.vhf -w H:/Robot/Handset/Handset/encoder.sch
--Design Name: encoder
--Device: xc9500xl
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity NOR8_MXILINX_encoder is
   port ( I0 : in    std_logic; 
          I1 : in    std_logic; 
          I2 : in    std_logic; 
          I3 : in    std_logic; 
          I4 : in    std_logic; 
          I5 : in    std_logic; 
          I6 : in    std_logic; 
          I7 : in    std_logic; 
          O  : out   std_logic);
end NOR8_MXILINX_encoder;

architecture BEHAVIORAL of NOR8_MXILINX_encoder is
   attribute BOX_TYPE   : string ;
   signal S0 : std_logic;
   signal S1 : std_logic;
   component OR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR4 : component is "BLACK_BOX";
   
   component NOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NOR2 : component is "BLACK_BOX";
   
begin
   I_36_110 : OR4
      port map (I0=>I0,
                I1=>I1,
                I2=>I2,
                I3=>I3,
                O=>S0);
   
   I_36_127 : OR4
      port map (I0=>I4,
                I1=>I5,
                I2=>I6,
                I3=>I7,
                O=>S1);
   
   I_36_140 : NOR2
      port map (I0=>S0,
                I1=>S1,
                O=>O);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity encoder is
   port ( D0        : in    std_logic; 
          D1        : in    std_logic; 
          D2        : in    std_logic; 
          D3        : in    std_logic; 
          D4        : in    std_logic; 
          D5        : in    std_logic; 
          D6        : in    std_logic; 
          D7        : in    std_logic; 
          Encoder_X : out   std_logic; 
          Encoder_Y : out   std_logic; 
          Encoder_Z : out   std_logic);
end encoder;

architecture BEHAVIORAL of encoder is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal XLXN_121  : std_logic;
   component NOR8_MXILINX_encoder
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             I5 : in    std_logic; 
             I6 : in    std_logic; 
             I7 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   component OR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR4 : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_63 : label is "XLXI_63_0";
begin
   XLXI_63 : NOR8_MXILINX_encoder
      port map (I0=>D7,
                I1=>D6,
                I2=>D5,
                I3=>D4,
                I4=>D3,
                I5=>D2,
                I6=>D1,
                I7=>D0,
                O=>XLXN_121);
   
   XLXI_67 : OR4
      port map (I0=>D4,
                I1=>D3,
                I2=>D0,
                I3=>XLXN_121,
                O=>Encoder_X);
   
   XLXI_68 : OR4
      port map (I0=>D5,
                I1=>D3,
                I2=>D1,
                I3=>XLXN_121,
                O=>Encoder_Y);
   
   XLXI_69 : OR4
      port map (I0=>D5,
                I1=>D2,
                I2=>D0,
                I3=>XLXN_121,
                O=>Encoder_Z);
   
end BEHAVIORAL;


