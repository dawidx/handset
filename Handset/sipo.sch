<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="xc9500xl" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_4" />
        <signal name="XLXN_3" />
        <signal name="XLXN_17" />
        <signal name="XLXN_23" />
        <signal name="XLXN_36" />
        <signal name="XLXN_37" />
        <signal name="L8" />
        <signal name="L7" />
        <signal name="L6" />
        <signal name="XLXN_16" />
        <signal name="XLXN_15" />
        <signal name="L5" />
        <signal name="L4" />
        <signal name="L3" />
        <signal name="L1" />
        <signal name="L2" />
        <signal name="XLXN_75" />
        <signal name="XLXN_76" />
        <signal name="XLXN_77" />
        <signal name="XLXN_79" />
        <signal name="XLXN_80" />
        <signal name="XLXN_87" />
        <signal name="XLXN_109" />
        <signal name="XLXN_112" />
        <port polarity="Output" name="L8" />
        <port polarity="Output" name="L7" />
        <port polarity="Output" name="L6" />
        <port polarity="Output" name="L5" />
        <port polarity="Output" name="L4" />
        <port polarity="Output" name="L3" />
        <port polarity="Output" name="L1" />
        <port polarity="Output" name="L2" />
        <port polarity="Input" name="XLXN_112" />
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="fd" name="XLXI_1">
            <blockpin signalname="XLXN_109" name="C" />
            <blockpin signalname="XLXN_112" name="D" />
            <blockpin signalname="XLXN_3" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_2">
            <blockpin signalname="XLXN_109" name="C" />
            <blockpin signalname="XLXN_3" name="D" />
            <blockpin signalname="XLXN_4" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_3">
            <blockpin signalname="XLXN_109" name="C" />
            <blockpin signalname="XLXN_4" name="D" />
            <blockpin signalname="XLXN_15" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_4">
            <blockpin signalname="XLXN_109" name="C" />
            <blockpin signalname="XLXN_15" name="D" />
            <blockpin signalname="XLXN_16" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_5">
            <blockpin signalname="XLXN_109" name="C" />
            <blockpin signalname="XLXN_16" name="D" />
            <blockpin signalname="XLXN_17" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_7">
            <blockpin signalname="XLXN_109" name="C" />
            <blockpin signalname="XLXN_17" name="D" />
            <blockpin signalname="XLXN_23" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_8">
            <blockpin signalname="XLXN_80" name="C" />
            <blockpin signalname="XLXN_3" name="D" />
            <blockpin signalname="L8" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_9">
            <blockpin signalname="XLXN_80" name="C" />
            <blockpin signalname="XLXN_4" name="D" />
            <blockpin signalname="L7" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_12">
            <blockpin signalname="XLXN_80" name="C" />
            <blockpin signalname="XLXN_17" name="D" />
            <blockpin signalname="L4" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_13">
            <blockpin signalname="XLXN_109" name="C" />
            <blockpin signalname="XLXN_23" name="D" />
            <blockpin signalname="XLXN_36" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_26">
            <blockpin signalname="XLXN_109" name="C" />
            <blockpin signalname="XLXN_36" name="D" />
            <blockpin signalname="XLXN_37" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_53">
            <blockpin signalname="XLXN_80" name="C" />
            <blockpin signalname="XLXN_23" name="D" />
            <blockpin signalname="L3" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_54">
            <blockpin signalname="XLXN_80" name="C" />
            <blockpin signalname="XLXN_36" name="D" />
            <blockpin signalname="L2" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_10">
            <blockpin signalname="XLXN_80" name="C" />
            <blockpin signalname="XLXN_15" name="D" />
            <blockpin signalname="L6" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_11">
            <blockpin signalname="XLXN_80" name="C" />
            <blockpin signalname="XLXN_16" name="D" />
            <blockpin signalname="L5" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_55">
            <blockpin signalname="XLXN_80" name="C" />
            <blockpin signalname="XLXN_37" name="D" />
            <blockpin signalname="L1" name="Q" />
        </block>
        <block symbolname="inv" name="XLXI_78">
            <blockpin signalname="XLXN_77" name="I" />
            <blockpin signalname="XLXN_87" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_79">
            <blockpin signalname="XLXN_79" name="I" />
            <blockpin signalname="XLXN_80" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_77">
            <blockpin signalname="XLXN_75" name="I" />
            <blockpin signalname="XLXN_76" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_100">
            <blockpin signalname="XLXN_109" name="C" />
            <blockpin signalname="XLXN_76" name="D" />
            <blockpin signalname="XLXN_75" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_101">
            <blockpin signalname="XLXN_76" name="C" />
            <blockpin signalname="XLXN_87" name="D" />
            <blockpin signalname="XLXN_77" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_102">
            <blockpin signalname="XLXN_87" name="C" />
            <blockpin signalname="XLXN_80" name="D" />
            <blockpin signalname="XLXN_79" name="Q" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="912" y="448" name="XLXI_1" orien="R0" />
        <instance x="1408" y="448" name="XLXI_2" orien="R0" />
        <instance x="1872" y="448" name="XLXI_3" orien="R0" />
        <instance x="2320" y="448" name="XLXI_4" orien="R0" />
        <instance x="2768" y="448" name="XLXI_5" orien="R0" />
        <instance x="3216" y="448" name="XLXI_7" orien="R0" />
        <branch name="XLXN_4">
            <wire x2="1824" y1="192" y2="192" x1="1792" />
            <wire x2="1824" y1="192" y2="1088" x1="1824" />
            <wire x2="1872" y1="192" y2="192" x1="1824" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="1344" y1="192" y2="192" x1="1296" />
            <wire x2="1344" y1="192" y2="1088" x1="1344" />
            <wire x2="1408" y1="192" y2="192" x1="1344" />
        </branch>
        <instance x="1344" y="1344" name="XLXI_8" orien="R0" />
        <instance x="1824" y="1344" name="XLXI_9" orien="R0" />
        <instance x="3264" y="1344" name="XLXI_12" orien="R0" />
        <branch name="XLXN_17">
            <wire x2="3184" y1="192" y2="192" x1="3152" />
            <wire x2="3216" y1="192" y2="192" x1="3184" />
            <wire x2="3184" y1="192" y2="1088" x1="3184" />
            <wire x2="3264" y1="1088" y2="1088" x1="3184" />
        </branch>
        <instance x="3664" y="448" name="XLXI_13" orien="R0" />
        <branch name="XLXN_23">
            <wire x2="3616" y1="192" y2="192" x1="3600" />
            <wire x2="3664" y1="192" y2="192" x1="3616" />
            <wire x2="3616" y1="192" y2="464" x1="3616" />
            <wire x2="3712" y1="464" y2="464" x1="3616" />
            <wire x2="3712" y1="464" y2="1088" x1="3712" />
            <wire x2="3760" y1="1088" y2="1088" x1="3712" />
        </branch>
        <branch name="XLXN_36">
            <wire x2="4096" y1="192" y2="192" x1="4048" />
            <wire x2="4144" y1="192" y2="192" x1="4096" />
            <wire x2="4096" y1="192" y2="464" x1="4096" />
            <wire x2="4208" y1="464" y2="464" x1="4096" />
            <wire x2="4208" y1="464" y2="1088" x1="4208" />
            <wire x2="4256" y1="1088" y2="1088" x1="4208" />
        </branch>
        <instance x="4144" y="448" name="XLXI_26" orien="R0" />
        <instance x="3760" y="1344" name="XLXI_53" orien="R0" />
        <instance x="4256" y="1344" name="XLXI_54" orien="R0" />
        <branch name="XLXN_37">
            <wire x2="4688" y1="192" y2="192" x1="4528" />
            <wire x2="4688" y1="192" y2="1136" x1="4688" />
            <wire x2="4720" y1="1136" y2="1136" x1="4688" />
        </branch>
        <branch name="L8">
            <wire x2="1760" y1="1088" y2="1088" x1="1728" />
        </branch>
        <branch name="L7">
            <wire x2="2224" y1="1088" y2="1088" x1="2208" />
        </branch>
        <branch name="L6">
            <wire x2="2688" y1="1088" y2="1088" x1="2656" />
            <wire x2="2688" y1="1088" y2="1120" x1="2688" />
            <wire x2="2688" y1="1120" y2="1120" x1="2672" />
        </branch>
        <branch name="XLXN_16">
            <wire x2="2736" y1="192" y2="192" x1="2704" />
            <wire x2="2768" y1="192" y2="192" x1="2736" />
            <wire x2="2736" y1="192" y2="1088" x1="2736" />
            <wire x2="2752" y1="1088" y2="1088" x1="2736" />
        </branch>
        <branch name="XLXN_15">
            <wire x2="2272" y1="192" y2="192" x1="2256" />
            <wire x2="2320" y1="192" y2="192" x1="2272" />
            <wire x2="2272" y1="192" y2="1088" x1="2272" />
        </branch>
        <instance x="2272" y="1344" name="XLXI_10" orien="R0" />
        <branch name="L5">
            <wire x2="3168" y1="1088" y2="1088" x1="3136" />
            <wire x2="3168" y1="1088" y2="1120" x1="3168" />
            <wire x2="3168" y1="1120" y2="1120" x1="3152" />
        </branch>
        <instance x="2752" y="1344" name="XLXI_11" orien="R0" />
        <branch name="L4">
            <wire x2="3680" y1="1088" y2="1088" x1="3648" />
        </branch>
        <branch name="L3">
            <wire x2="4176" y1="1088" y2="1088" x1="4144" />
        </branch>
        <branch name="L1">
            <wire x2="5168" y1="1136" y2="1136" x1="5104" />
            <wire x2="5168" y1="1088" y2="1088" x1="5152" />
            <wire x2="5168" y1="1088" y2="1136" x1="5168" />
        </branch>
        <branch name="L2">
            <wire x2="4672" y1="1088" y2="1088" x1="4640" />
        </branch>
        <iomarker fontsize="28" x="1760" y="1088" name="L8" orien="R180" />
        <iomarker fontsize="28" x="2224" y="1088" name="L7" orien="R180" />
        <iomarker fontsize="28" x="2672" y="1120" name="L6" orien="R180" />
        <iomarker fontsize="28" x="3152" y="1120" name="L5" orien="R180" />
        <iomarker fontsize="28" x="3680" y="1088" name="L4" orien="R180" />
        <iomarker fontsize="28" x="4176" y="1088" name="L3" orien="R180" />
        <iomarker fontsize="28" x="4672" y="1088" name="L2" orien="R180" />
        <iomarker fontsize="28" x="5152" y="1088" name="L1" orien="R180" />
        <instance x="4720" y="1392" name="XLXI_55" orien="R0" />
        <branch name="XLXN_75">
            <wire x2="736" y1="720" y2="720" x1="688" />
        </branch>
        <branch name="XLXN_76">
            <wire x2="224" y1="640" y2="720" x1="224" />
            <wire x2="224" y1="720" y2="1264" x1="224" />
            <wire x2="320" y1="1264" y2="1264" x1="224" />
            <wire x2="304" y1="720" y2="720" x1="224" />
            <wire x2="1024" y1="640" y2="640" x1="224" />
            <wire x2="1024" y1="640" y2="720" x1="1024" />
            <wire x2="1024" y1="720" y2="720" x1="960" />
        </branch>
        <instance x="768" y="1168" name="XLXI_78" orien="R0" />
        <branch name="XLXN_77">
            <wire x2="768" y1="1136" y2="1136" x1="704" />
        </branch>
        <instance x="752" y="1536" name="XLXI_79" orien="R0" />
        <branch name="XLXN_79">
            <wire x2="752" y1="1504" y2="1504" x1="704" />
        </branch>
        <instance x="736" y="752" name="XLXI_77" orien="R0" />
        <branch name="XLXN_109">
            <wire x2="256" y1="544" y2="848" x1="256" />
            <wire x2="304" y1="848" y2="848" x1="256" />
            <wire x2="896" y1="544" y2="544" x1="256" />
            <wire x2="1392" y1="544" y2="544" x1="896" />
            <wire x2="1856" y1="544" y2="544" x1="1392" />
            <wire x2="2304" y1="544" y2="544" x1="1856" />
            <wire x2="2752" y1="544" y2="544" x1="2304" />
            <wire x2="3200" y1="544" y2="544" x1="2752" />
            <wire x2="3648" y1="544" y2="544" x1="3200" />
            <wire x2="4064" y1="544" y2="544" x1="3648" />
            <wire x2="912" y1="320" y2="320" x1="896" />
            <wire x2="896" y1="320" y2="544" x1="896" />
            <wire x2="1408" y1="320" y2="320" x1="1392" />
            <wire x2="1392" y1="320" y2="544" x1="1392" />
            <wire x2="1872" y1="320" y2="320" x1="1856" />
            <wire x2="1856" y1="320" y2="544" x1="1856" />
            <wire x2="2304" y1="320" y2="544" x1="2304" />
            <wire x2="2320" y1="320" y2="320" x1="2304" />
            <wire x2="2752" y1="320" y2="544" x1="2752" />
            <wire x2="2768" y1="320" y2="320" x1="2752" />
            <wire x2="3200" y1="320" y2="544" x1="3200" />
            <wire x2="3216" y1="320" y2="320" x1="3200" />
            <wire x2="3648" y1="320" y2="544" x1="3648" />
            <wire x2="3664" y1="320" y2="320" x1="3648" />
            <wire x2="4144" y1="320" y2="320" x1="4064" />
            <wire x2="4064" y1="320" y2="544" x1="4064" />
        </branch>
        <instance x="304" y="976" name="XLXI_100" orien="R0" />
        <instance x="320" y="1392" name="XLXI_101" orien="R0" />
        <branch name="XLXN_87">
            <wire x2="992" y1="1008" y2="1008" x1="288" />
            <wire x2="1008" y1="1008" y2="1008" x1="992" />
            <wire x2="1008" y1="1008" y2="1136" x1="1008" />
            <wire x2="288" y1="1008" y2="1136" x1="288" />
            <wire x2="320" y1="1136" y2="1136" x1="288" />
            <wire x2="288" y1="1136" y2="1632" x1="288" />
            <wire x2="320" y1="1632" y2="1632" x1="288" />
            <wire x2="1008" y1="1136" y2="1136" x1="992" />
        </branch>
        <branch name="XLXN_80">
            <wire x2="1040" y1="1392" y2="1392" x1="256" />
            <wire x2="1040" y1="1392" y2="1504" x1="1040" />
            <wire x2="1056" y1="1504" y2="1504" x1="1040" />
            <wire x2="1792" y1="1504" y2="1504" x1="1056" />
            <wire x2="2256" y1="1504" y2="1504" x1="1792" />
            <wire x2="2720" y1="1504" y2="1504" x1="2256" />
            <wire x2="3184" y1="1504" y2="1504" x1="2720" />
            <wire x2="3728" y1="1504" y2="1504" x1="3184" />
            <wire x2="4224" y1="1504" y2="1504" x1="3728" />
            <wire x2="4704" y1="1504" y2="1504" x1="4224" />
            <wire x2="256" y1="1392" y2="1504" x1="256" />
            <wire x2="320" y1="1504" y2="1504" x1="256" />
            <wire x2="1040" y1="1504" y2="1504" x1="976" />
            <wire x2="1344" y1="1216" y2="1216" x1="1056" />
            <wire x2="1056" y1="1216" y2="1504" x1="1056" />
            <wire x2="1792" y1="1216" y2="1504" x1="1792" />
            <wire x2="1824" y1="1216" y2="1216" x1="1792" />
            <wire x2="2272" y1="1216" y2="1216" x1="2256" />
            <wire x2="2256" y1="1216" y2="1232" x1="2256" />
            <wire x2="2256" y1="1232" y2="1504" x1="2256" />
            <wire x2="2720" y1="1216" y2="1504" x1="2720" />
            <wire x2="2752" y1="1216" y2="1216" x1="2720" />
            <wire x2="3184" y1="1216" y2="1504" x1="3184" />
            <wire x2="3264" y1="1216" y2="1216" x1="3184" />
            <wire x2="3728" y1="1216" y2="1504" x1="3728" />
            <wire x2="3760" y1="1216" y2="1216" x1="3728" />
            <wire x2="4224" y1="1216" y2="1504" x1="4224" />
            <wire x2="4256" y1="1216" y2="1216" x1="4224" />
            <wire x2="4704" y1="1264" y2="1504" x1="4704" />
            <wire x2="4720" y1="1264" y2="1264" x1="4704" />
        </branch>
        <instance x="320" y="1760" name="XLXI_102" orien="R0" />
        <branch name="XLXN_112">
            <wire x2="912" y1="192" y2="192" x1="880" />
        </branch>
        <iomarker fontsize="28" x="880" y="192" name="XLXN_112" orien="R180" />
    </sheet>
</drawing>