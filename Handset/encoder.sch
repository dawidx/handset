<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="xc9500xl" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="D7" />
        <signal name="D6" />
        <signal name="D5" />
        <signal name="D4" />
        <signal name="D3" />
        <signal name="D2" />
        <signal name="D0" />
        <signal name="D1" />
        <signal name="BISTABLE_PISO_LOGIC" />
        <signal name="Encoder_X" />
        <signal name="Encoder_Y" />
        <signal name="Encoder_Z" />
        <port polarity="Input" name="D7" />
        <port polarity="Input" name="D6" />
        <port polarity="Input" name="D5" />
        <port polarity="Input" name="D4" />
        <port polarity="Input" name="D3" />
        <port polarity="Input" name="D2" />
        <port polarity="Input" name="D0" />
        <port polarity="Input" name="D1" />
        <port polarity="Output" name="BISTABLE_PISO_LOGIC" />
        <port polarity="Output" name="Encoder_X" />
        <port polarity="Output" name="Encoder_Y" />
        <port polarity="Output" name="Encoder_Z" />
        <blockdef name="nor8">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <arc ex="48" ey="-336" sx="48" sy="-240" r="56" cx="16" cy="-288" />
            <line x2="64" y1="-336" y2="-336" x1="128" />
            <line x2="64" y1="-240" y2="-240" x1="128" />
            <arc ex="208" ey="-288" sx="128" sy="-240" r="88" cx="132" cy="-328" />
            <arc ex="128" ey="-336" sx="208" sy="-288" r="88" cx="132" cy="-248" />
            <line x2="228" y1="-288" y2="-288" x1="256" />
            <circle r="10" cx="218" cy="-286" />
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="48" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-384" y2="-384" x1="0" />
            <line x2="48" y1="-448" y2="-448" x1="0" />
            <line x2="48" y1="-512" y2="-512" x1="0" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="48" y1="-336" y2="-512" x1="48" />
            <line x2="48" y1="-64" y2="-240" x1="48" />
            <line x2="48" y1="-336" y2="-336" x1="72" />
            <line x2="52" y1="-240" y2="-240" x1="72" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <block symbolname="nor8" name="XLXI_63">
            <blockpin signalname="D7" name="I0" />
            <blockpin signalname="D6" name="I1" />
            <blockpin signalname="D5" name="I2" />
            <blockpin signalname="D4" name="I3" />
            <blockpin signalname="D3" name="I4" />
            <blockpin signalname="D2" name="I5" />
            <blockpin signalname="D1" name="I6" />
            <blockpin signalname="D0" name="I7" />
            <blockpin signalname="BISTABLE_PISO_LOGIC" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_67">
            <blockpin signalname="D4" name="I0" />
            <blockpin signalname="D3" name="I1" />
            <blockpin signalname="D0" name="I2" />
            <blockpin signalname="BISTABLE_PISO_LOGIC" name="I3" />
            <blockpin signalname="Encoder_X" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_68">
            <blockpin signalname="D5" name="I0" />
            <blockpin signalname="D3" name="I1" />
            <blockpin signalname="D1" name="I2" />
            <blockpin signalname="BISTABLE_PISO_LOGIC" name="I3" />
            <blockpin signalname="Encoder_Y" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_69">
            <blockpin signalname="D5" name="I0" />
            <blockpin signalname="D2" name="I1" />
            <blockpin signalname="D0" name="I2" />
            <blockpin signalname="BISTABLE_PISO_LOGIC" name="I3" />
            <blockpin signalname="Encoder_Z" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <iomarker fontsize="28" x="224" y="960" name="D7" orien="R180" />
        <iomarker fontsize="28" x="224" y="896" name="D6" orien="R180" />
        <iomarker fontsize="28" x="224" y="832" name="D5" orien="R180" />
        <iomarker fontsize="28" x="224" y="768" name="D4" orien="R180" />
        <iomarker fontsize="28" x="224" y="704" name="D3" orien="R180" />
        <iomarker fontsize="28" x="224" y="640" name="D2" orien="R180" />
        <iomarker fontsize="28" x="224" y="576" name="D1" orien="R180" />
        <iomarker fontsize="28" x="224" y="512" name="D0" orien="R180" />
        <instance x="560" y="1456" name="XLXI_67" orien="R0" />
        <instance x="560" y="1696" name="XLXI_68" orien="R0" />
        <instance x="560" y="1936" name="XLXI_69" orien="R0" />
        <branch name="D7">
            <wire x2="496" y1="960" y2="960" x1="224" />
        </branch>
        <branch name="D6">
            <wire x2="496" y1="896" y2="896" x1="224" />
        </branch>
        <branch name="D5">
            <wire x2="352" y1="832" y2="832" x1="224" />
            <wire x2="496" y1="832" y2="832" x1="352" />
            <wire x2="352" y1="832" y2="1632" x1="352" />
            <wire x2="560" y1="1632" y2="1632" x1="352" />
            <wire x2="352" y1="1632" y2="1872" x1="352" />
            <wire x2="560" y1="1872" y2="1872" x1="352" />
        </branch>
        <branch name="D4">
            <wire x2="336" y1="768" y2="768" x1="224" />
            <wire x2="496" y1="768" y2="768" x1="336" />
            <wire x2="336" y1="768" y2="1392" x1="336" />
            <wire x2="352" y1="1392" y2="1392" x1="336" />
            <wire x2="560" y1="1392" y2="1392" x1="352" />
        </branch>
        <branch name="D3">
            <wire x2="320" y1="704" y2="704" x1="224" />
            <wire x2="496" y1="704" y2="704" x1="320" />
            <wire x2="320" y1="704" y2="1312" x1="320" />
            <wire x2="320" y1="1312" y2="1328" x1="320" />
            <wire x2="560" y1="1328" y2="1328" x1="320" />
            <wire x2="320" y1="1328" y2="1568" x1="320" />
            <wire x2="560" y1="1568" y2="1568" x1="320" />
        </branch>
        <branch name="D2">
            <wire x2="304" y1="640" y2="640" x1="224" />
            <wire x2="496" y1="640" y2="640" x1="304" />
            <wire x2="304" y1="640" y2="1808" x1="304" />
            <wire x2="560" y1="1808" y2="1808" x1="304" />
        </branch>
        <branch name="D1">
            <wire x2="288" y1="576" y2="576" x1="224" />
            <wire x2="288" y1="576" y2="1504" x1="288" />
            <wire x2="560" y1="1504" y2="1504" x1="288" />
            <wire x2="496" y1="576" y2="576" x1="288" />
        </branch>
        <branch name="D0">
            <wire x2="272" y1="512" y2="512" x1="224" />
            <wire x2="272" y1="512" y2="1264" x1="272" />
            <wire x2="560" y1="1264" y2="1264" x1="272" />
            <wire x2="272" y1="1264" y2="1744" x1="272" />
            <wire x2="560" y1="1744" y2="1744" x1="272" />
            <wire x2="496" y1="512" y2="512" x1="272" />
        </branch>
        <instance x="496" y="1024" name="XLXI_63" orien="R0" />
        <branch name="BISTABLE_PISO_LOGIC">
            <wire x2="800" y1="1040" y2="1040" x1="480" />
            <wire x2="480" y1="1040" y2="1200" x1="480" />
            <wire x2="560" y1="1200" y2="1200" x1="480" />
            <wire x2="480" y1="1200" y2="1440" x1="480" />
            <wire x2="560" y1="1440" y2="1440" x1="480" />
            <wire x2="480" y1="1440" y2="1680" x1="480" />
            <wire x2="560" y1="1680" y2="1680" x1="480" />
            <wire x2="800" y1="736" y2="736" x1="752" />
            <wire x2="800" y1="736" y2="1040" x1="800" />
            <wire x2="1104" y1="736" y2="736" x1="800" />
        </branch>
        <branch name="Encoder_X">
            <wire x2="848" y1="1296" y2="1296" x1="816" />
        </branch>
        <iomarker fontsize="28" x="848" y="1296" name="Encoder_X" orien="R0" />
        <branch name="Encoder_Y">
            <wire x2="848" y1="1536" y2="1536" x1="816" />
        </branch>
        <iomarker fontsize="28" x="848" y="1536" name="Encoder_Y" orien="R0" />
        <branch name="Encoder_Z">
            <wire x2="848" y1="1776" y2="1776" x1="816" />
        </branch>
        <iomarker fontsize="28" x="848" y="1776" name="Encoder_Z" orien="R0" />
        <iomarker fontsize="28" x="1104" y="736" name="BISTABLE_PISO_LOGIC" orien="R0" />
    </sheet>
</drawing>